#!/bin/sh

export DOMAIN=${DOMAIN:-$(hostname --domain)}
export DEBUG=${DEBUG}
#export EMAIL=${EMAIL}
EMAIL=tharangar@opensource.lk
SITE=coppermail.dyndns.org
DEBUG=true
export DOMAIN=${DOMAIN}
# creating certificate files for openldap server


#chmod -R 755 /etc/openldap/certs/
#export KEY_PATH=/etc/openldap/certs/
#files=$(shopt -s nullglob dotglob; echo $KEY_PATH)
#echo $KEY_PATH
#echo "Checking for existing certificates"

#cd  /etc/openldap/certs/

# https://www.itzgeek.com/how-tos/linux/centos-how-tos/configure-openldap-with-ssl-on-centos-7-rhel-7.html
if [ "$DEBUG" = true ]; then
   #mkdir -p $KEY_PATH
   # method 1 ------------------------------------------------------------------
   #openssl req -nodes -x509 -newkey rsa:4096 -keyout ${KEY_PATH}.privkey.pem -out ${KEY_PATH}.fullchain.pem -days 365 -subj "/C=US/ST=Oregon/L=Portland/O=Company Name/OU=Org/CN=nextgenmed.dyndns.org"
   #openssl req -x509 -days 365 -sha256 -newkey rsa:2048 -nodes -keyout /etc/ssl/private/$SITE.key -out /etc/ssl/certs/$SITE-cert.pem -subj "/C=LK/ST=Colombo/L=Maradana/O=Company Name/OU=Org/CN=coppermail.dyndns.org"
   
   # method 2 -----------------------------------------------------------------------
   #openssl genrsa -out coppermailrootCA.key 2048
   #openssl req -x509 -new -nodes -key coppermailrootCA.key -sha256 -days 1024 -out coppermailrootCA.pem -subj "/C=LK/ST=Colombo/L=Maradana/O=Company Name/OU=Org/CN=coppermail.dyndns.org"
   #openssl genrsa -out coppermailldap.key 2048
   #openssl req -new -key coppermailldap.key -out coppermailldap.csr -subj "/C=LK/ST=Colombo/L=Maradana/O=Company Name/OU=Org/CN=coppermail.dyndns.org"
   #openssl x509 -req -in coppermailldap.csr -CA coppermailrootCA.pem -CAkey coppermailrootCA.key -CAcreateserial -out coppermailldap.crt -days 1460 -sha256

   # method 3 ----------------------------------------------------------------------
   #Create a private key for the Certificate Authority: 
    sh -c "certtool --generate-privkey > /etc/ssl/private/cakey.pem"
    #Create the template/file /etc/ssl/ca.info to define the CA: 
    rm -rf /etc/ssl/ca.info
    
    {
     echo 'cn = coppermail'
     echo 'ca'
     echo 'cert_signing_key'
    }>> /etc/ssl/ca.info
   # Create the self-signed CA certificate: 
   certtool --generate-self-signed --load-privkey /etc/ssl/private/cakey.pem --template /etc/ssl/ca.info --outfile /etc/ssl/certs/cacert.pem
   # Make a private key for the server: 
   #Replace ldap01 in the filename with your server's hostname. Naming the certificate and key for the host and service that will be using them will help keep things clear. 
    certtool --generate-privkey --bits 1024 --outfile /etc/ssl/private/ldap01_slapd_key.pem
   # Create the /etc/ssl/ldap01.info info file containing: 
   # The bellow certificate is good for 10 years. Adjust accordingly. 
   rm -rf /etc/ssl/ldap01.info
   {
    echo 'organization = coppermail'
    echo 'cn = ldap01.coppermail.dyndns.org'
    echo 'tls_www_server'
    echo 'encryption_key'
    echo 'signing_key'
    echo 'expiration_days = 3650'
   }>> /etc/ssl/ldap01.info
   # Create the server's certificate: 
   certtool --generate-certificate \
    --load-privkey /etc/ssl/private/ldap01_slapd_key.pem \
    --load-ca-certificate /etc/ssl/certs/cacert.pem \
    --load-ca-privkey /etc/ssl/private/cakey.pem \
    --template /etc/ssl/ldap01.info \
    --outfile /etc/ssl/certs/ldap01_slapd_cert.pem
   # Adjust permissions and ownership: 

   chgrp openldap /etc/ssl/private/ldap01_slapd_key.pem
   chmod 0640 /etc/ssl/private/ldap01_slapd_key.pem
   gpasswd -a openldap ssl-cert

 

   echo "IN DEBUG MODE!!!! - GENERATED SELF SIGNED SSL KEY" ll /etc/openldap/certs/coppermail*
  else
if (( ${#files} )); then
       echo "Found existing keys!!"
   else
       echo "No Certicates Found!!"
       echo "Generating SSL Certificates with LetsEncrypt"
       letsencrypt certonly --standalone -d $SITE --noninteractive --agree-tos --email $EMAIL
       if (( ${#files} )); then
         echo "Certicate generation Successfull"
       else
         echo "Certicate generation failed."
         exit 1
       fi
   fi
  fi

# move to the correct let's encrypt directory
#cd /etc/letsencrypt/live/$SITE

# copy the files related to letsencript
#cp cert.pem /etc/ssl/certs/$SITE.cert.pem
#cp fullchain.pem /etc/ssl/certs/$SITE.fullchain.pem
#cp privkey.pem /etc/ssl/private/$SITE.privkey.pem

# adjust permissions of the private key
#chown :ssl-cert /etc/ssl/private/$SITE.privkey.pem
#chmod 640 /etc/ssl/private/$SITE.privkey.pem
# -----------------------------------------------------
# copy files generated from openssl
#cp /etc/ssl/certs/$SITE-cert.pem /etc/ssl/certs/$SITE.cert.pem
#cp *fullchain.pem /etc/ssl/certs/$SITE.fullchain.pem
#cp /etc/ssl/private/$SITE.key /etc/ssl/private/$SITE.privkey.pem

# adjust permissions of the private key
#chown :ssl-cert /etc/ssl/private/$SITE.key
#chmod 640 /etc/ssl/private/$SITE.key


# clearing errors
#apt-get -f install
#rm -rf /var/cache/debconf/*.dat
#dpkg-reconfigure slapd

# Adding postfix plugin



# phpldapadmin configuration
#sed -i 's/127.0.0.1/172.19.0.21/' /etc/phpldapadmin/config.php
sed -i 's/cn=admin,dc=example,dc=com/cn=admin,dc=coppermail,dc=dyndns,dc=org/' /etc/phpldapadmin/config.php
sed -i 's/dc=example,dc=com/dc=coppermail,dc=dyndns,dc=org/' /etc/phpldapadmin/config.php

# copy conf files to ldap
#cp -rf /etc/openldap/ldap/* /etc/ldap/

# restart slapd to load new certificates
#service slapd start
service apache2 start


# exicute ldif configurations
cd /etc/openldap/ldif

wget -O postfix.ldif https://raw.githubusercontent.com/68b32/postfix-ldap-schema/master/postfix.ldif

#ldapadd -ZZ -x -W -D cn=admin,cn=config -H ldap://172.19.0.21 -f postfix.ldif


# add a user
#ldapadd -x -D cn=admin,dc=coppermail,dc=dyndns,dc=org -W -f add_content.ldif
 
   # enableing ssl/tsl
   #ldapmodify -Y EXTERNAL -H ldapi:/// -f certinfo.ldif
   # enabl loging
   #ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f logging.ldif

#rm -rf /var/cache/debconf/*.dat
 tail -f /dev/null
